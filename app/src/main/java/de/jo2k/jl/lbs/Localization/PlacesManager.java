package de.jo2k.jl.lbs.Localization;

import android.net.wifi.ScanResult;
import android.util.Log;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class manages all places and captures by the user. It also calculates the user's position.
 */

public class PlacesManager implements Serializable {
    ArrayList<Place> places = new ArrayList<>();
    private final String TAG = "PlacesManager";

    public PlacesManager() {
        this.reset();
    }

    /**
     * Resets list of user's places to built-in defaults.
     */
    public void reset() {
        Log.i(TAG, "Resetting places ...");
        places = new ArrayList<>();
        places.add(new Place("Home"));
        places.add(new Place("Work"));
        places.add(new Place("University"));
    }

    public void addPlace(String name) {
        places.add(new Place(name));
    }

    public Place getPlace(int index) {
        return places.get(index);
    }

    public String[] getPlaces() {
        String[] placeNames = new String[places.size()];

        for (int i = 0; i < places.size(); i++)
            placeNames[i] = places.get(i).getName();

        return placeNames;
    }

    public void removePlace(int index) {
        places.remove(index);
    }

    /**
     * A wrapper function to choose one of the multiple localization-methods.
     */
    public String localizeUser(int method, List<ScanResult> currentScanResults) {
        switch (method) {
            case 1:
                return localizationMethod1(currentScanResults);
            case 2:
                return localizationMethod2(currentScanResults);
            default:
                throw new IllegalArgumentException("An undefined localization-method has been called.");
        }
    }

    /**
     * Simple fingerprinting-algorithm to localize the user, given a list of scans.
     *
     * @param currentScanResults All ScanResults, which were captured by the user at this very moment.
     * @return The position, the user is currently at.
     */
    private String localizationMethod1(List<ScanResult> currentScanResults) {
        String bestMatch = null; // stays null, if localization fails. Will be filled with name of place otherwise.
        int cur_intersect = 0;   // counts the current intersections between to sets of ScanResults.
        int top_intersect = 0;   // stores the top amount of intersections found.

        if (currentScanResults == null)
            return null;

        ArrayList<String> scan_bssids = new ArrayList<>();

        // create scanresult for current position
        for (ScanResult sc : currentScanResults) {
            scan_bssids.add(sc.BSSID);
        }

        // traverse places and results of scans
        for (Place place : places) {
            for (Scan scan : place.getScans()) {
                for (ScanCapture sc : scan.getScanCaptureList()) {
                    if (scan_bssids.contains(sc.getMacAddress())) {
                        cur_intersect++;
                    }
                }

                // compare
                if (cur_intersect > top_intersect) {
                    bestMatch = place.getName() + " (" + cur_intersect + ")"; // set best match
                    top_intersect = cur_intersect;
                }

                cur_intersect = 0;
            }
        }

        return bestMatch;
    }

    /**
     * New method for wifi fingerprinting. Basically, only the TOP 33% of all measurements are being
     * taken into consideration (at least 5, or less, when there are less than five measurements).
     * Fingerprinting happens upon consideration of minimal euclidean distance.
     * @param currentScanResults Provided ScanResult-List.
     * @return Returns a String, at which place the user is currently at.
     */
    private String localizationMethod2(List<ScanResult> currentScanResults) {
        if (currentScanResults == null || currentScanResults.size() == 0)
            return null;

        Scan currentScan = new Scan();

        for (int i = 0; i < currentScanResults.size(); i++)
            currentScan.addScancapture(currentScanResults.get(i).SSID, currentScanResults.get(i).BSSID, Integer.toString(currentScanResults.get(i).level));

        // stores pair of (PlaceName, avgScan)
        Map<String, Scan> referenceScans = new HashMap<>();

        // collect all averageScans. This is the set, we will use fingerprinting later on.
        for (Place place : places) {
            if (place.getAverageScan().getScanCaptureList() != null || place.getAverageScan().getScanCaptureList().size() != 0) {
                referenceScans.put(place.getName(), place.getAverageScan());
            }
        }

        // Sort currentScan.
        // Scans obtained by getAverageScan() are already sorted.
        Collections.sort(currentScan.getScanCaptureList());

        // Now we reduce the size of our sets to the upper third, which should be at least 5 ScanCaptures.
        // If there are less than five available ScanCaptures, we do not reduze at all.

        int reduction = currentScan.getScanCaptureList().size();
        if (reduction > 5 && reduction < 15) {
            reduction = 5;
        } else if (reduction >= 15) {
            reduction = Math.round(reduction / 3);
        }

        // Since everything is sorted by now, we execute the reduction to the top elements
        // Also we remove all measurements, where RSSI is < -80 (faulty)
        currentScan.setScanCaptureList(new ArrayList<ScanCapture>(currentScan.getScanCaptureList().subList(0, reduction)));
        currentScan.removeFaultyCaptures();
        for (Scan value : referenceScans.values()) {
            if (value.getScanCaptureList().size() != 0) {
                value.setScanCaptureList(new ArrayList<ScanCapture>(value.getScanCaptureList().subList(0, reduction)));
                value.removeFaultyCaptures();
            }

            // Do the actual fingerprinting by least-mean-square-error-comparison:
            Double leastDistance = null;
            String winner = null;

            for (Map.Entry<String, Scan> entry : referenceScans.entrySet()) {
                Double currentEuclideanDistance = currentScan.getEuclideanDistance(entry.getValue());

                if (currentEuclideanDistance == null) {
                    // sets are disjoint
                    // do nothing, i.e. continue
                    continue;
                } else if (leastDistance == null) {
                    // sets are not disjoint and MSE was computed initially:
                    leastDistance = currentEuclideanDistance;
                    winner = entry.getKey();
                } else if (currentEuclideanDistance < leastDistance) {
                    leastDistance = currentEuclideanDistance;
                    winner = entry.getKey();
                }
            }
            Log.i("LocMethod2", "Euclidean Distance: "+leastDistance);
            NumberFormat formatter = new DecimalFormat("#0.0");

            if (leastDistance == null) {
                return null;
            } else {
                return winner + " (" + formatter.format(leastDistance) + " | " + currentScan.getScanCaptureList().size() + ")";
            }
        }

        return null;
    }
}