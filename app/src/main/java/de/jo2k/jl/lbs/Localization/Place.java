package de.jo2k.jl.lbs.Localization;

import android.app.Activity;
import android.net.wifi.ScanResult;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class represents a place.
 * Each place can contain one or more Scans.
 */

public class Place extends Activity implements Serializable {
    ArrayList<Scan> scans = new ArrayList<Scan>();
    String name;

    Place(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public void deleteScan(int index){
        scans.remove(index);
    }

    public ArrayList<Scan> getScans(){
        return scans;
    }

    public String[] getScansToString(){
        String[] placeNames = new String[scans.size()];

        for(int i = 0; i < scans.size(); i++)
            placeNames[i] = scans.get(i).getTimestamp();

        return placeNames;
    }

    public void processScanList(){
        scans.add(new Scan());
    }

    public Scan getScan(int index){
        return scans.get(index);
    }

    /**
     * Obtains a List of ScanResults and parses it into a POJO-set of ScanCaptures.
     * @param results The list to transform into POJOs
     */
    public void processScanList(List<ScanResult> results){
        Comparator<ScanResult> comparator = new Comparator<ScanResult>() {
            @Override
            public int compare(ScanResult lhs, ScanResult rhs) {
                return (lhs.level > rhs.level ? -1 : (lhs.level == rhs.level ? 1 : 0));
            }
        };
        Collections.sort(results, comparator);

        if(results == null)
            return;

        Scan sc = new Scan();

        for(ScanResult r : results){
            sc.addScancapture(r.SSID, r.BSSID, Integer.toString(r.level));
        }

        scans.add(sc);
    }

    /**
     * Returns a Scan, which is the average of all collected scans.
     * This scan contains only of ScanCaptures, which have been present to all Scans.
     * Also the RSSI of each ScanCapture is arithemetically averaged over all measurements.
     * @return A scan, with averaged and sorted (by RSSI) captures.
     */
    public Scan getAverageScan(){
        Scan scan = new Scan(); // create an empty scan

        Map<String, ArrayList<String>> rssimap = new HashMap<String, ArrayList<String>>();
        Map<String, String> macAdressMap = new HashMap<String, String>();
        // rssimap will contain a pair of ["<macAdress>", ["RSSI1", "RSSI2", "RSSI3", ...]]

        for (Scan s : scans) { // for each scan
            for (ScanCapture sc : s.getScanCaptureList()){ // for every capture in this scan
                macAdressMap.put(sc.getMacAddress(), sc.getSSID());

                if (rssimap.get(sc.getMacAddress()) == null) { // check if Mac is already in rssimap
                    ArrayList<String> al = new ArrayList<String>();
                    al.add(sc.getRSSI());
                    rssimap.put(sc.getMacAddress(), al);
                } else {
                    // get present arraylist for key from rssimap and append element
                    ArrayList<String> al = rssimap.get(sc.getMacAddress());
                    al.add(sc.getRSSI());
                }
            }
        }

        // rssimap contains now key-value pairs of each macAdress with all measured RSSI-values

        // Iterate over rssimap to a) remove all macAdresses, which are not present to all Scans and
        // b) create an average-RSSI for each macAdress.
        Iterator it = rssimap.entrySet().iterator();
        while (it.hasNext()) { // iterate over all known mac-adresses
            Map.Entry pair = (Map.Entry) it.next();

            ArrayList<String> rssiValues = (ArrayList<String>) pair.getValue();

            // there should be exactly one value for each Scan. Otherwise this macAdress is missing
            // from some Scans. Remove it in this case.
            if(rssiValues.size() != scans.size() || rssiValues.size() == 0) {
                it.remove(); // macAdress was not present in all Scans or not present at all
                continue;
            }

            // calculate arithmetic average
            int avgRSSI = 0;
            for (String s : rssiValues)
                avgRSSI += Integer.parseInt(s);
            avgRSSI = Math.round(avgRSSI / rssiValues.size());

            String macAdress = (String) pair.getKey();

            scan.addScancapture(macAdressMap.get(macAdress), macAdress, Integer.toString(avgRSSI));
        }

        Collections.sort(scan.getScanCaptureList());

        return scan; // This Scan now consists of a ScanCapture for each discovered MacAdress,
                     // where the RSSI is averaged. Also it is sorted by RSSI.
    }


    public void removeScan(int index){
        scans.remove(index);
    }

}
