package de.jo2k.jl.lbs.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ListFragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import de.jo2k.jl.lbs.Activities.MainActivity;
import de.jo2k.jl.lbs.Localization.PlacesManager;
import de.jo2k.jl.lbs.R;

/**
 * PlacesFragment stores all user-defined places i.e. locations.
 */

public class PlacesFragment extends ListFragment {
    View view;
    ArrayAdapter<String> adapter;
    PlacesManager plm;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        plm = ((MainActivity)getActivity()).getPlacesManager();

        refreshList();

        view = inflater.inflate(R.layout.fragment_places, container, false);

        // Action handlers:
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.places_FAB);
        fab.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view){
                addPlacePopup();
            }
        });


        ListView lv = (ListView) view.findViewById(android.R.id.list);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                listItemLongClick(pos);
                return true;
            }
        });

        return view;
    }

    private void addPlacePopup(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add a new place");

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                plm.addPlace(input.getText().toString());
                refreshList();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void listItemLongClick(final int index){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Delete entry \"" + plm.getPlace(index).getName() + "\"");
        builder.setMessage("Are you sure?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                plm.removePlace(index);
                refreshList();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    void refreshList(){
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, plm.getPlaces());
        setListAdapter(adapter);
    }

}


