package de.jo2k.jl.lbs.Localization;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * A scancapture is a tuple of an ssid, the bssid and rssi.
 * This class simply stores those values.
 */

public class ScanCapture implements Serializable, Comparable<ScanCapture> {
    private String ssid;
    private String bssid;
    private String rssi;

    ScanCapture(String ssid, String bssid, String rssi){
        this.ssid = ssid;
        this.bssid = bssid;
        this.rssi = rssi;
    }

    String getSSID(){
        return ssid;
    }

    String getMacAddress(){
        return bssid;
    }

    String getRSSI(){
        return rssi;
    }

    int getRSSIToInt(){
        return Integer.parseInt(rssi);
    }

    @Override
    public int compareTo(@NonNull ScanCapture other) {
        if (this.getRSSIToInt() > other.getRSSIToInt()) { // ex. -50 < -80
            return -1;
        }

        if (this.getRSSIToInt() < other.getRSSIToInt()) { // ex. -50 < -80
            return 1;
        }

        return 0;
    }
}
