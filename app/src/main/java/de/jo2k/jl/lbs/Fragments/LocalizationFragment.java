package de.jo2k.jl.lbs.Fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import de.jo2k.jl.lbs.Activities.MainActivity;
import de.jo2k.jl.lbs.Localization.PlacesManager;
import de.jo2k.jl.lbs.R;

import static android.content.Context.SENSOR_SERVICE;

/**
 * The LocalizationFragment localizes the user by the defined places and captured scans.
 * It also features a little powersave-mode, accessing the device's accelerometer.
 */

public class LocalizationFragment extends Fragment implements SensorEventListener {
    View view;
    FloatingActionButton fab;

    PlacesManager plm;

    WifiManager wifi;
    WifiScanReceiver wifiReciever;

    TextView tv, tv2, tv3, tv4;

    SensorManager sensorManager;

    private final String TAG = "LocalizationFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        plm = ((MainActivity)getActivity()).getPlacesManager();
        view = inflater.inflate(R.layout.fragment_localization, container, false);

        // Action handlers:
        fab = (FloatingActionButton) view.findViewById(R.id.l10n_FAB);

        tv = (TextView) view.findViewById(R.id.l10n_TV);
        tv2 = (TextView) view.findViewById(R.id.l10n_TV2);
        tv3 = (TextView) view.findViewById(R.id.l10n_TV3);
        tv4 = (TextView) view.findViewById(R.id.l10n_TV4);

        wifi = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiReciever =  new WifiScanReceiver();

        fab.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view){
                wifi.startScan();
                setPowersaveMode(true);
            }
        });

        if (!wifi.isWifiEnabled()){
            Toast.makeText(getContext(), R.string.enable_wifi, Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }

        sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);

        registerReceiver();

        return view;
    }
    void setPowersaveMode(boolean powersave){
        if(!powersave){
            fab.setEnabled(true);
            fab.show();
            tv3.setText(""); //emptystr
        }

        if(powersave){
            fab.setEnabled(false);
            fab.hide();
            tv3.setText(R.string.enter_powersave);
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(wifiReciever);
        sensorManager.unregisterListener(this);
    }


    public void registerReceiver() {
        getActivity().registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }

    public class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            // Permission stuff:
            try {
                if (getActivity() != null && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0x0);
                } else {
                    List<ScanResult> wifiScanList = wifi.getScanResults();
                    Log.i(TAG, "LocalizationFragment: received " + wifiScanList.size() + " results!");
                    tv.setText(getString(R.string.localizing_user) + " " + wifiScanList.size() + " " + getString(R.string.localizing_user2));

                    String userloc1 = plm.localizeUser(1, wifiScanList);
                    if(userloc1 == null){
                        tv2.setText(getString(R.string.method1) + "  " + getString(R.string.position_not_found));
                    } else {
                        tv2.setText(getString(R.string.method1) + "  " + getString(R.string.userposition) + " " + userloc1 + ".");
                    }

                    String userloc2 = plm.localizeUser(2, wifiScanList);
                    if(userloc1 == null){
                        tv4.setText(getString(R.string.method2) + "  " + getString(R.string.position_not_found));
                    } else {
                        tv4.setText(getString(R.string.method2) + "  " + getString(R.string.userposition) + " " + userloc2 + ".");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {    }

    /**
     * Disables the powersave-mode, when phone is picked up.
     * @param event event, which is provided when the Sensor changes.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            double ax = event.values[0];
            double ay = event.values[1];
            double az = event.values[2];

            // double abssum = Math.abs(ax)+Math.abs(ay)+Math.abs(az);
            double err = Math.sqrt(Math.pow(ax, 2) + Math.pow(ay, 2) + Math.pow(az, 2));

            // Log.i(TAG, "Powersave-Sensorchange: " + err);

            if(err >= 10) { // >= 14 (sum)
                setPowersaveMode(false);
            }
        }
    }
}