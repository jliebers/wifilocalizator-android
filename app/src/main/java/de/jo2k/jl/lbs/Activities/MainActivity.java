package de.jo2k.jl.lbs.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import de.jo2k.jl.lbs.Fragments.LocalizationFragment;
import de.jo2k.jl.lbs.Fragments.PlacesFragment;
import de.jo2k.jl.lbs.Fragments.ScanFragment;
import de.jo2k.jl.lbs.Localization.PlacesManager;
import de.jo2k.jl.lbs.R;

/**
 * MainActivity. Functions are implemented in Fragments.
 * This activity spawns the FragmentManager and the bottomNavigation.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private BottomNavigationView bottomNavigation;
    private Fragment fragment;
    private android.support.v4.app.FragmentManager fragmentManager;
    PlacesManager plm;

    int current_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        plm = new PlacesManager();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigation = (BottomNavigationView) findViewById(R.id.navigation);
        fragmentManager = getSupportFragmentManager();

        // switch to start fragment:
        if(savedInstanceState == null) {
            fragment = new PlacesFragment();
            final android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment).commit();
        }

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if(id != current_id) {
                    switch (id){
                        case R.id.navigation_places:
                            fragment = new PlacesFragment();
                            break;
                        case R.id.navigation_scans:
                            fragment = new ScanFragment();
                            break;
                        case R.id.navigation_l10n:
                            fragment = new LocalizationFragment();
                            break;
                    }

                    final android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.main_container, fragment).commit();
                } else {
                    return false;
                }

                current_id = id;
                return true;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i(TAG, "Saving instance state ... ");
        try {
            FileOutputStream fos = getApplicationContext().openFileOutput("plm", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(plm);
            os.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        Log.i(TAG, "success.");
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i(TAG, "Restoring instance state ... ");

        try {
            FileInputStream fis = getApplicationContext().openFileInput("plm");
            ObjectInputStream is = new ObjectInputStream(fis);
            plm = (PlacesManager) is.readObject();
            is.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        Log.i(TAG, "success.");
    }

    public PlacesManager getPlacesManager(){
        return this.plm;
    }
}