package de.jo2k.jl.lbs.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import de.jo2k.jl.lbs.Activities.MainActivity;
import de.jo2k.jl.lbs.Localization.PlacesManager;
import de.jo2k.jl.lbs.R;

/**
 * The ScanFragments implements the capturing of new scans by the user.
 */

public class ScanFragment extends ListFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    View view;
    PlacesManager plm;
    Spinner sItems;
    WifiManager wifi;
    WifiScanReceiver wifiReciever;

    private final String TAG = "ScanFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_scan, container, false);

        plm = ((MainActivity)getActivity()).getPlacesManager();

        String[] spinnerArray = plm.getPlaces();

        sItems = (Spinner) view.findViewById(R.id.places_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sItems.setAdapter(adapter);

        // Action handlers:
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.scans_FAB);
        fab.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view){
                wifi.startScan();
                Toast.makeText(getContext(), "Scanning ...", Toast.LENGTH_SHORT).show();
                refreshList();
            }
        });


        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                refreshList();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // empty
            }
        });


        // LIST VIEW:
        ListView lv = (ListView) view.findViewById(android.R.id.list);
        lv.setOnItemClickListener(this);
        lv.setOnItemLongClickListener(this);

        // WiFi Stuff:
        wifi = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiReciever =  new WifiScanReceiver();

        if (!wifi.isWifiEnabled()){
            Toast.makeText(getContext(), "Enabling WiFi ...", Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }

        registerReceiver();

        refreshList();
        return view;
    }

    public void registerReceiver() {
        getActivity().registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }


    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long id) {
        Toast.makeText(getContext(), plm.getPlace(sItems.getSelectedItemPosition()).getScansToString()[pos], Toast.LENGTH_SHORT).show();
        refreshList();
    }


    public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
        final int index = pos;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Delete set \"" + plm.getPlace(sItems.getSelectedItemPosition()).getScansToString()[index] + "\"?");

        builder.setMessage(plm.getPlace(sItems.getSelectedItemPosition()).getScan(index).getScanCaptureListToString());

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                plm.getPlace(sItems.getSelectedItemPosition()).removeScan(index);
                refreshList();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

        refreshList();
        return true;
    }



    void refreshList(){
        if(plm.getPlace(sItems.getSelectedItemPosition()).getScansToString() == null)
            return;

        try {
            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, plm.getPlace(sItems.getSelectedItemPosition()).getScansToString());
            setListAdapter(adapter2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            // Set up permissions:
            try {
                if (getActivity() != null && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0x0);
                } else {
                    List<ScanResult> wifiScanList = wifi.getScanResults();

                    if(wifiScanList.size() == 0){
                        Log.i(TAG, "Got 0 results!");
                        Toast.makeText(getContext(), getString(R.string.err_zeroresults), Toast.LENGTH_LONG).show();
                    } else {
                        Log.i(TAG, "Got " + wifiScanList.size() + " results!");
                    }
                    plm.getPlace(sItems.getSelectedItemPosition()).processScanList(wifiScanList);
                }
                refreshList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0x12345) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            Log.i(TAG, "Get wifi");
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(wifiReciever);
    }
}
