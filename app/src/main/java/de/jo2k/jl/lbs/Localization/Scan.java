package de.jo2k.jl.lbs.Localization;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A Scan is the result of a wifi-scan initiated by the user.
 * Each tuple of SSID, macAdress and RSSI is stored in a ScanCapture.
 * A Place has multiple Scans.
 */

public class Scan implements Serializable {
    private String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    private ArrayList<ScanCapture> scanCaptures = new ArrayList<>();

    public String getTimestamp(){
        return timeStamp.toString() + " (" + scanCaptures.size() + ")";
    }

    /**
     * Adds a ScanCapture to the set of existing ScanCaptures.
     * Filters the Scancapture by certain criteria.
     * @param ssid Contains the human-readable network-name.
     * @param macAdress Contains the BSSID, i.e. the macAdress or another unique identifier.
     * @param rssi received signal strength indicator. The higher, the better.
     */
    public void addScancapture(String ssid, String macAdress, String rssi) {
        scanCaptures.add(new ScanCapture(ssid, macAdress, rssi));
    }

    public ArrayList<ScanCapture> getScanCaptureList(){
        return scanCaptures;
    }

    public void setScanCaptureList(ArrayList<ScanCapture> newScanCaptureList){
        scanCaptures = newScanCaptureList;
    }

    public String getScanCaptureListToString(){
        if(scanCaptures.size() == 0)
            return "No recorded captures.";

        String ret = "";
        int cnt = 1;

        for (ScanCapture sc : scanCaptures){
            ret += cnt++ + ")\n";
            ret += "  SSID: " + sc.getSSID() + "\n";
            ret += "  MAC:  " + sc.getMacAddress() + "\n";
            ret += "  RSSI: " + sc.getRSSI() +"\n";
            ret += "\n";
        }

        return ret.substring(0, ret.length() - 2);
    }

    /**
     * Computes the Mean-Square-Error (MSE) in comparison to another Scan.
     * @param other Other Scan-object, to which the euclidean distance should be calculated.
     * @return the calculated euclidean distance.
     */
    public Double getEuclideanDistance(Scan other){
        Map<String, Integer> otherMap = new HashMap<>();

        ArrayList<Double> squares = new ArrayList<>();

        for(ScanCapture sc : other.getScanCaptureList()){
            otherMap.put(sc.getMacAddress(), sc.getRSSIToInt());
        }

        for(ScanCapture sc : this.scanCaptures){
            if(otherMap.get(sc.getMacAddress()) != null) {
                double otherRSSI = otherMap.get(sc.getMacAddress());
                double diff = sc.getRSSIToInt() - otherRSSI;
                double square = Math.pow(diff, 2);

                squares.add(square);
            }
        }

        if(squares.size() == 0) // nothing found
            return null;

        double sum = 0;
        for(Double d : squares)
            sum += d;

        return sum / squares.size();
    }

    /**
     * Removes all faulty captures, where RSSI is smaller than -80, since they are not reliable.
     */
    public void removeFaultyCaptures() {
        for(int i = 0; i < scanCaptures.size(); i++){
            if(scanCaptures.get(i).getRSSIToInt() < -80)
                scanCaptures.remove(i);
        }
    }
}
