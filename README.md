# wifilocalizator-android

A simple Android application which performs indoor localization by taking fingerprints of the surrounding SSIDs.
The user can define places, whose WiFi-fingerprints are taken and localize themselves later on.
